import { Component, Input } from '@angular/core';


@Component({
    moduleId:module.id,
    selector:'foto',
    //templateUrl:'./app/foto/foto.component.html' // Sem o moduleId:module.id é assim que deve ser
    templateUrl:'./foto.component.html' // com o moduleId:module.id usa-se apenas o caminho relativo,
})
export class FotoComponent{
    
    @Input() titulo: string;
    @Input() url: string;
    descricao: string;
}