import { FiltroPorTitulo } from './foto.pipes';
import { NgModule } from '@angular/core';
import { FotoComponent } from './foto.component';

@NgModule({
    //tudo que eu faz parte do modulo eu preciso declarar
    declarations: [FotoComponent, FiltroPorTitulo], 
    exports:[FotoComponent, FiltroPorTitulo]
})
export class FotoModule{

}