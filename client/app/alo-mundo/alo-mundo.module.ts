import { NgModule } from "@angular/core";
import { AloMundoComponent } from "./ola-mundo.component";


@NgModule({
    declarations:[AloMundoComponent],
    exports:[AloMundoComponent]
})
export class AloMundoModule{

}